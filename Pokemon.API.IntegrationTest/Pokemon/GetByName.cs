﻿using Application.Pokemon.Commands.Queries;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Pokemon.API.IntegrationTest.Pokemon
{   
    public class GetByName 
    {
        private readonly HttpClient _httpClient;

        public GetByName()
        {
            var appFactory = new WebApplicationFactory<Startup>();
            _httpClient = appFactory.CreateClient();
           
        }

        /// <summary>
        /// I don't like this but this is a brittle test i can't control and I may hit limits - should fail but.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task when_calling_API_with_valid_pokemon_name_response_is_for_expected_pokemon()
        {
            var pokemonName = "charmander";

            var response = await _httpClient.GetAsync($"/pokemon/{pokemonName}");

            if (response.StatusCode.Equals(HttpStatusCode.TooManyRequests))
            {
                Assert.Equal(HttpStatusCode.TooManyRequests, response.StatusCode);
            }
            else
            {
              

                var responseContent = await response.Content.ReadAsStringAsync();

                var pokemonVM = JsonSerializer.Deserialize<PokemonVM>(responseContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

                Assert.Equal(pokemonName, pokemonVM.Name);
                
            }
        }

        [Fact]
        public async Task GivenInvalid_Pokemon_name_ReturnsNotFoundStatusCode()
        {
            var pokemonName = "BobBlahx";

            var response = await _httpClient.GetAsync($"/pokemon/{pokemonName}");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }

}
