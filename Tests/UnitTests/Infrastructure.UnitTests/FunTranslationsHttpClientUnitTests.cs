﻿using FluentAssertions;
using Infrastructure.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.UnitTests
{
    public class FunTranslationsHttpClientUnitTests
    {
        protected FunTranslationsHttpClient _funTranslationsHttpClient;
        protected readonly Mock<IHttpClientFactory> _httpClientFactoryMock = new Mock<IHttpClientFactory>();
        protected readonly Mock<HttpMessageHandler> _messageHandlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict); //mock message handler response for http client.
        protected HttpResponseMessage _httpResponseMessage;
        protected readonly Mock<ILogger<FunTranslationsHttpClient>> _loggerMock = new Mock<ILogger<FunTranslationsHttpClient>>();

        public FunTranslationsHttpClientUnitTests()
        {
            var translationResponse = @"{
                ""success"": {
                    ""total"": 1
                },
                ""contents"": {
                    ""translated"": ""Obviously prefers hot places. At which hour 't rains,  steam is did doth sayeth to spout from the tip of its tail."",
                    ""text"": ""Obviously prefers hot places. When it rains, steam is said to spout from the tip of its tail."",
                    ""translation"": ""shakespeare""
                }
            }";

            _httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(translationResponse)
            };


            _messageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(_httpResponseMessage)
                .Verifiable();

            var httpClient = new HttpClient(_messageHandlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com")
            };

            _httpClientFactoryMock.Setup(x => x.CreateClient("blah any"))
                 .Returns(httpClient);

            _funTranslationsHttpClient = new FunTranslationsHttpClient(httpClient, _loggerMock.Object);

        }

        [Fact]
        public async Task TranslateToShakespeareAsync_not_null_when_valid_response_returned()
        {
            // Act
            var response = await _funTranslationsHttpClient.TranslateToShakespeareAsync("blah", new CancellationToken());

            response.Should().NotBeNull();

        }

        [Fact]
        public async Task TranslateToShakespeareAsync_is_called_with_the_expected_uri_exactly_once()
        {
            // Act
            var response = await _funTranslationsHttpClient.TranslateToShakespeareAsync("blah", new CancellationToken());

            var expectedUri = new Uri("http://example.com/translate/shakespeare.json");

            _messageHandlerMock.Protected().Verify(
               "SendAsync",
               Times.Exactly(1), // we expected a single external request
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post  // we expected a POST request
                  && req.RequestUri == expectedUri // to this uri
               ),
               ItExpr.IsAny<CancellationToken>()
            );

        }

        [Fact]
        public async Task TranslateToShakespeareAsync_throws_NotFoundException_when_StatusCode_is_not_success()
        {


            // arrange failing test
            _httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.BadRequest,
                Content = new StringContent(@"{ ""name"":""blah"", ""unexpectedJson"":""yep""}")
            };


            _messageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(_httpResponseMessage)
                .Verifiable();

            var httpClient = new HttpClient(_messageHandlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com")
            };

            _httpClientFactoryMock.Setup(x => x.CreateClient("blah any"))
                 .Returns(httpClient);

            _funTranslationsHttpClient = new FunTranslationsHttpClient(httpClient, _loggerMock.Object);

            async Task Result() => await _funTranslationsHttpClient.TranslateToShakespeareAsync("blah", new CancellationToken());

            // Assert
            var exception = await Record.ExceptionAsync(Result);
            exception.Should()
                .BeOfType<HttpRequestException>();
        }

    }
}
