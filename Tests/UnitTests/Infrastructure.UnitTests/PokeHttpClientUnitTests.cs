using Application.Exceptions;
using FluentAssertions;
using Infrastructure.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Infrastructure.UnitTests
{
    public class PokeHttpClientUnitTests
    {
        protected PokeHttpClient _pokeHttpClient;
        protected readonly Mock<IHttpClientFactory> _httpClientFactoryMock = new Mock<IHttpClientFactory>();
        protected readonly Mock<HttpMessageHandler> _messageHandlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict); //mock message handler response for http client.
        protected HttpResponseMessage _httpResponseMessage;
        protected readonly Mock<ILogger<PokeHttpClient>> _loggerMock = new Mock<ILogger<PokeHttpClient>>();


        public PokeHttpClientUnitTests()
        {
            var speciesResponseJson = @"{""Id"":4,""Name"":""charmander"",""flavor_text_entries"":[{""flavor_text"":""Obviously prefers\nhot places. When\nit rains, steam\fis said to spout\nfrom the tip of\nits tail."",""Language"":{""Name"":""en"",""Url"":""https://pokeapi.co/api/v2/language/9/""},""Version"":{""Name"":""red"",""Url"":""https://pokeapi.co/api/v2/version/1/""}},{""flavor_text"":""Obviously prefers\nhot places. When\nit rains, steam\fis said to spout\nfrom the tip of\nits tail."",""Language"":{""Name"":""en"",""Url"":""https://pokeapi.co/api/v2/language/9/""},""Version"":{""Name"":""blue"",""Url"":""https://pokeapi.co/api/v2/version/2/""}}]}";

            _httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(speciesResponseJson)
            };


            _messageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(_httpResponseMessage)
                .Verifiable();

            var httpClient = new HttpClient(_messageHandlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com")
            };

            _httpClientFactoryMock.Setup(x => x.CreateClient("blah any"))
                 .Returns(httpClient);

            _pokeHttpClient = new PokeHttpClient(httpClient, _loggerMock.Object);

        }

        [Fact]
        public async Task GetPokemonSpeciesAsync_not_null_when_valid_response()
        {
            // Act
            var response = await _pokeHttpClient.GetPokemonSpeciesAsync("blah", new CancellationToken());

            response.Should().NotBeNull();

        }

        [Fact]
        public async Task GetPokemonSpeciesAsync_is_called_with_the_expected_uri_exactly_once()
        {
            // Act
            var response = await _pokeHttpClient.GetPokemonSpeciesAsync("blah", new CancellationToken());

            var expectedUri = new Uri("http://example.com/api/v2/pokemon-species/blah");

            _messageHandlerMock.Protected().Verify(
               "SendAsync",
               Times.Exactly(1), // we expected a single external request
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get  // we expected a GET request
                  && req.RequestUri == expectedUri // to this uri
               ),
               ItExpr.IsAny<CancellationToken>()
            );

        }

        [Fact]
        public async Task GetPokemonSpeciesAsync_throws_JsonException_when_response_invalidJson()
        {


            // arrange failing test
            _httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(@"{ ""name"":""blah"", ""unexpectedJson"":""yep""")
            };


            _messageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(_httpResponseMessage)
                .Verifiable();

            var httpClient = new HttpClient(_messageHandlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com")
            };

            _httpClientFactoryMock.Setup(x => x.CreateClient("blah any"))
                 .Returns(httpClient);

            _pokeHttpClient = new PokeHttpClient(httpClient, _loggerMock.Object);

            async Task Result() => await _pokeHttpClient.GetPokemonSpeciesAsync("blah", new CancellationToken());

            // Assert
            var exception = await Record.ExceptionAsync(Result);
            exception.Should()
                .BeOfType<JsonException>();
        }

        [Fact]
        public async Task GetPokemonSpeciesAsync_throws_Exception_when_StatusCode_is_not_success()
        {


            // arrange failing test
            _httpResponseMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.BadRequest,
                Content = new StringContent(@"{ ""name"":""blah"", ""unexpectedJson"":""yep""}")
            };


            _messageHandlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(_httpResponseMessage)
                .Verifiable();

            var httpClient = new HttpClient(_messageHandlerMock.Object)
            {
                BaseAddress = new Uri("http://example.com")
            };

            _httpClientFactoryMock.Setup(x => x.CreateClient("blah any"))
                 .Returns(httpClient);

            _pokeHttpClient = new PokeHttpClient(httpClient, _loggerMock.Object);

            async Task Result() => await _pokeHttpClient.GetPokemonSpeciesAsync("blah", new CancellationToken());

            // Assert
            var exception = await Record.ExceptionAsync(Result);
            exception.Should()
                .BeOfType<NotFoundException>();
        }
    }
}
