﻿using Application.Pokemon.Commands.Queries;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Pokemon.API.Controllers;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Pokemon.API.UnitTests.Controllers
{
    public abstract class PokemonControllerUnitTests
    {
        public abstract class PokemonControllerUnitTest
        {
            protected readonly Mock<IMediator> _mediatorMock = new Mock<IMediator>();
            protected readonly Mock<ILogger<PokemonController>> _loggerMock = new Mock<ILogger<PokemonController>>();
            protected readonly PokemonController _pokemonController;

            protected PokemonControllerUnitTest()
            {
                //arrange
                _pokemonController = new PokemonController(_mediatorMock.Object, _loggerMock.Object);

                PokemonVM pokemonVM = new PokemonVM();

                _mediatorMock.Setup(x => x.Send(It.IsAny<GetPokemonDescriptionQuery>(), It.IsAny<CancellationToken>())).ReturnsAsync(pokemonVM);

            }
        }

        public class Get_Pokemon_string : PokemonControllerUnitTest
        {
            [Fact]
            public async Task Successful_request_returns_status200OK()
            {
                // Act
                var response = await _pokemonController.Get("name", new CancellationToken());

                var result = response.Result.As<OkObjectResult>();

                // assert
                result.StatusCode.Should().Be(StatusCodes.Status200OK);
            }

            [Fact]
            public async Task Successful_request_returns_expected_responseType_PokemonVM()
            {
                // Act
                var response = await _pokemonController.Get("name", new CancellationToken());

                var result = response.Result.As<OkObjectResult>();

                // assert
                result.Value.Should().BeOfType<PokemonVM>();
            }
        }
    }
}
