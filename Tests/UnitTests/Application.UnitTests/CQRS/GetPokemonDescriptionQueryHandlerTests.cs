﻿using Application.Common.Interfaces;
using Application.Common.Models.FunTranslations;
using Application.Common.Models.Poke;
using Application.Pokemon.Commands.Queries;
using FluentAssertions;
using Moq;
using Moq.Protected;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Application.UnitTests.CQRS
{
    public abstract class GetPokemonDescriptionQueryHandlerTests
    {
        public abstract class GetPokemonDescriptionQueryHandlerTest
        {
            protected readonly Mock<IPokeHttpClient> _pokeHttpClient = new Mock<IPokeHttpClient>();
            protected readonly Mock<IFunTranslationsHttpClient> _funTranslationsHttpClient = new Mock<IFunTranslationsHttpClient>();
            private readonly GetPokemonDescriptionQueryHandler _getPokemonDescriptionQueryHandler;
            private readonly GetPokemonDescriptionQuery _getPokemonDescriptionQuery;

            protected GetPokemonDescriptionQueryHandlerTest()
            {

                // Arrange

                var text = "This is\nmy text";
                _getPokemonDescriptionQuery = new GetPokemonDescriptionQuery { PokemonName = "bob" };

                _getPokemonDescriptionQueryHandler = new GetPokemonDescriptionQueryHandler(_pokeHttpClient.Object, _funTranslationsHttpClient.Object);

                var flavourTextEntries = new List<FlavorTextEntry>();
                flavourTextEntries.Add(new FlavorTextEntry { FlavorText = text, Language = new ApiItem { Name = "en" } });
                var pokemonSpeciesResponse = new PokemonSpeciesResponse { Id = 1, Name = "pokemonName", FlavorTextEntries = flavourTextEntries };

                _pokeHttpClient.Setup(x => x.GetPokemonSpeciesAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(pokemonSpeciesResponse);

                var funTranslationResponse = new FunTranslationResponse { Contents = new Contents { Text = text, Translated = "shakes", Translation = "The translation" } };
                _funTranslationsHttpClient.Setup(x => x.TranslateToShakespeareAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(funTranslationResponse);

            }

            public class GetDescription : GetPokemonDescriptionQueryHandlerTest
            {

                [Fact]
                public async Task Query_with_populated_pokemonName_returns_exected_type_Pokemon_view_model()
                {

                    // Act
                    var response = await _getPokemonDescriptionQueryHandler.Handle(_getPokemonDescriptionQuery, new CancellationToken());

                    // Assert
                    response.Should().BeOfType<PokemonVM>();
                }

                [Fact]
                public async Task When_GetPokemonSpeciesAsync_returns_not_found_Then_404_exception_is_thrown()
                {

                    _pokeHttpClient.Setup(x => x.GetPokemonSpeciesAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ThrowsAsync(new HttpRequestException());

                    async Task Result() => await _getPokemonDescriptionQueryHandler.Handle(_getPokemonDescriptionQuery, new CancellationToken());

                    // Assert
                    var exception = await Record.ExceptionAsync(Result);
                    exception.Should()
                        .BeOfType<HttpRequestException>();
                }

                [Fact]
                public async Task Query_calls_pokeHttpclient_GetPokemonSpeciesAsync_Once()
                {

                    // Act
                    var response = await _getPokemonDescriptionQueryHandler.Handle(_getPokemonDescriptionQuery, new CancellationToken());

                    _pokeHttpClient.Verify(x => x.GetPokemonSpeciesAsync(It.IsAny<string>(),It.IsAny<CancellationToken>()), Times.AtLeastOnce);

                }


                [Fact]
                public async Task Query_calls_funTranslationsHttpClient_TranslateToShakespeareAsync_Once()
                {

                    // Act
                    var response = await _getPokemonDescriptionQueryHandler.Handle(_getPokemonDescriptionQuery, new CancellationToken());

                    _funTranslationsHttpClient.Verify(x => x.TranslateToShakespeareAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.AtLeastOnce);

                }

                [Fact]
                public async Task Query_cleans_description_before_calling_TranslateToShakespeareAsync()
                {

                    var expectedMessage = "This is my text";
                    // Act
                    var response = await _getPokemonDescriptionQueryHandler.Handle(_getPokemonDescriptionQuery, new CancellationToken());

                    _funTranslationsHttpClient.Verify(x => x.TranslateToShakespeareAsync(expectedMessage, It.IsAny<CancellationToken>()), Times.AtLeastOnce);

                }

            }

        }
    }
}
