﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Exceptions
{
    public class RateLimitReached : Exception
    {
        public RateLimitReached(string name, object key)
            : base($"Rate Limit for \"{name}\" reached {key} ")
        {
        }
    }
}