﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Application.Common.Models.Poke
{
    public class PokemonSpeciesResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonPropertyName("flavor_text_entries")]
        public List<FlavorTextEntry> FlavorTextEntries { get; set; }
    }
}