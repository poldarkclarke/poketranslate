﻿namespace Application.Common.Models.Poke
{
    public class ApiItem
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
