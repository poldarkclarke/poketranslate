﻿using System.Text.Json.Serialization;

namespace Application.Common.Models.Poke
{
    public class FlavorTextEntry
    {
        [JsonPropertyName("flavor_text")]
        public string FlavorText { get; set; }

        public ApiItem Language { get; set; }

        public ApiItem Version { get; set; }
    }
}

