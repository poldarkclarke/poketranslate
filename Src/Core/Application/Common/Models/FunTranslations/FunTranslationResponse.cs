﻿using System.Text.Json.Serialization;

namespace Application.Common.Models.FunTranslations
{
    public class FunTranslationResponse
    {
        [JsonPropertyName("success")]
        public Success Success { get; set; }

        [JsonPropertyName("contents")]
        public Contents Contents { get; set; }
    }
}
