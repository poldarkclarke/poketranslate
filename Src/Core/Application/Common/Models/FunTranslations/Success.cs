﻿using System.Text.Json.Serialization;

namespace Application.Common.Models.FunTranslations
{
    public class Success
    {
        [JsonPropertyName("total")]
        public int Total { get; set; }
    }
}
