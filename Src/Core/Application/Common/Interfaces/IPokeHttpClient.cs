﻿using Application.Common.Models.Poke;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IPokeHttpClient
    {
        Task<PokemonSpeciesResponse> GetPokemonSpeciesAsync(string name, CancellationToken token);
    }
}
