﻿using Application.Common.Models.FunTranslations;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IFunTranslationsHttpClient
    {
        Task<FunTranslationResponse> TranslateToShakespeareAsync(string text, CancellationToken token);

    }
}
