﻿namespace Application.Pokemon.Commands.Queries
{
    public class PokemonVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
