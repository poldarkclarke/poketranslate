﻿using Application.Common.Interfaces;
using Application.Common.Models.FunTranslations;
using MediatR;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Pokemon.Commands.Queries
{
    public class GetPokemonDescriptionQueryHandler : IRequestHandler<GetPokemonDescriptionQuery, PokemonVM>
    {
        private readonly IPokeHttpClient _pokeHttpclient;
        private readonly IFunTranslationsHttpClient _funTranslationsHttpClient;
        private const string _language = "en";

        public GetPokemonDescriptionQueryHandler(IPokeHttpClient pokeHttpclient, IFunTranslationsHttpClient funTranslationsHttpClient)
        {
            _pokeHttpclient = pokeHttpclient;
            _funTranslationsHttpClient = funTranslationsHttpClient;
        }

        public async Task<PokemonVM> Handle(GetPokemonDescriptionQuery request, CancellationToken cancellationToken)
        {

            var species = await _pokeHttpclient.GetPokemonSpeciesAsync(request.PokemonName, cancellationToken);

            var description = species.FlavorTextEntries.FirstOrDefault(x => x.Language.Name.Equals(_language));

            // clean the description of some escapted characters.
            var cleanDescription = string.Join(" ", Regex.Split(description.FlavorText, @"(?:\r|\n|\f)"));

            var translation = await _funTranslationsHttpClient.TranslateToShakespeareAsync(cleanDescription, cancellationToken);

            return new PokemonVM { Name = species.Name, Description = translation.Contents.Translated };
            
        }



    }
}
