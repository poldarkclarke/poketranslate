﻿using MediatR;

namespace Application.Pokemon.Commands.Queries
{
    public class GetPokemonDescriptionQuery : IRequest<PokemonVM>
    {
        // GetPokemonDescriptionQueryHandler does the work
        public string PokemonName { get; set; }
    }
}
