﻿using Application.Common.Interfaces;
using Application.Common.Models.Poke;
using Application.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class PokeHttpClient : IPokeHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<PokeHttpClient> _logger;

        public PokeHttpClient(HttpClient httpClient, ILogger<PokeHttpClient> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
            SetDefaultHeaders();
        }

        public async Task<PokemonSpeciesResponse> GetPokemonSpeciesAsync(string name, CancellationToken token)
        {
            try
            {
                _logger.LogInformation($"start GetPokemonSpeciesAsync {name}");

                var httpResponse = await _httpClient.GetAsync($"api/v2/pokemon-species/{name}", token);
                httpResponse.EnsureSuccessStatusCode();

                var responseContent = await httpResponse.Content.ReadAsStringAsync();

                var pokemonSpecies = JsonSerializer.Deserialize<PokemonSpeciesResponse>(responseContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

                string jsonString;
                jsonString = JsonSerializer.Serialize(pokemonSpecies);

                return pokemonSpecies;

            }
            catch(HttpRequestException e)
            {
                _logger.LogError(e, $"Error GetPokemonSpeciesAsync {name}");
                throw new NotFoundException("Pokemon", name);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error unexpected GetPokemonSpeciesAsync {name}");
                throw;
            }
            finally
            {
                _logger.LogInformation($"completed GetPokemonSpeciesAsync {name}");
            }

        }

        private void SetDefaultHeaders()
        {
            // add any headers we may need - could add API key if signed up
            _httpClient.DefaultRequestHeaders.Accept.Clear();
        }
    }
}
