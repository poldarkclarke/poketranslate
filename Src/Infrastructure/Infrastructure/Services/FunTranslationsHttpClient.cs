﻿using Application.Common.Interfaces;
using Application.Common.Models.FunTranslations;
using Application.Exceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class FunTranslationsHttpClient : IFunTranslationsHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<FunTranslationsHttpClient> _logger;

        public FunTranslationsHttpClient(HttpClient httpClient, ILogger<FunTranslationsHttpClient> logger)
        {
            _httpClient = httpClient;
            _logger = logger;
            SetDefaultHeaders();
        }

        public async Task<FunTranslationResponse> TranslateToShakespeareAsync(string text, CancellationToken token)
        {
            try
            {
                //todo maybe add the route to config and pass in strongly typed settings object
                _logger.LogInformation("calling out to translation API ");

                var parameters = new Dictionary<string, string> { { "text", text } };
                var encodedContent = new FormUrlEncodedContent(parameters);

                var httpResponse = await _httpClient.PostAsync("translate/shakespeare.json", encodedContent).ConfigureAwait(false);

                //specific check for rate limit
                //System.Net.HttpStatusCode.TooManyRequests
                if(httpResponse.StatusCode.Equals(HttpStatusCode.TooManyRequests))
                {
                    throw new RateLimitReached("translate/shakespeare.json", "");
                }

                httpResponse.EnsureSuccessStatusCode();

                var responseContent = await httpResponse.Content.ReadAsStringAsync();

                return JsonSerializer.Deserialize<FunTranslationResponse>(responseContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

            }
            catch (Exception e)
            {
                _logger.LogError("FunTranslationResponse ERROR", e.Message);
                throw;
            }
        }

        private void SetDefaultHeaders()
        {
            // add any headers we may need - could add API key if signed up
            _httpClient.DefaultRequestHeaders.Accept.Clear();
        }
    }
}
