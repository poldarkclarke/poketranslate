﻿using Application.Common.Interfaces;
using Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddHttpClient<IPokeHttpClient, PokeHttpClient>(client =>
            {
                client.BaseAddress = new Uri(configuration["PokemonApiSettings:BaseUrl"]);
            });

            services.AddHttpClient<IFunTranslationsHttpClient, FunTranslationsHttpClient>(client =>
            {
                client.BaseAddress = new Uri(configuration["ShakespearApiSettings:BaseUrl"]);
            });

            return services;
        }
    }
}