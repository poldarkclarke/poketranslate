﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Pokemon.Commands.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Pokemon.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PokemonController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<PokemonController> _logger;

        public PokemonController(IMediator mediator, ILogger<PokemonController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// Gets the description of a given pokemon
        /// </summary>
        /// <param name="pokemonName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("{pokemonName}")]
        public async Task<ActionResult<PokemonVM>> Get(string pokemonName, CancellationToken cancellationToken)
        {
            var query = new GetPokemonDescriptionQuery { PokemonName = pokemonName };
            var vm = await _mediator.Send(query, cancellationToken);
            return Ok(vm);

        }
    }
}
