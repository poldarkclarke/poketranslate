# Pokemon task

I used the CQRS pattern with Mediatr following the principles set out by Jason Taylor in his clean architecture presentation ([https://www.youtube.com/watch?v=_lwCVE_XgqI](https://www.youtube.com/watch?v=_lwCVE_XgqI))  

I create unit and integration tests as requested but due to the rate limits on the call to the shakespear translate API the integration test is brittle. 

You should be able to view my docker history from bitbucket - select commits and the master branch (https://bitbucket.org/poldarkclarke/poketranslate/commits/)

I only mapped the properties from the Pokemon species response that I needed.

A Docker file is included in the repo. I've included the steps to run it below.

## API Tests
Screenshot showing successful tests
![Screenshot](readme_files/Tests.PNG)

Valid Pokemon
![Screenshot](readme_files/DockerRun.PNG)

Invalid Pokemon
![Screenshot](readme_files/NotFound.PNG)

Rate limit Reached
![Screenshot](readme_files/RateLimit.PNG)

## Docker Image

The command to build the images (assuming you have docker installed)

```
docker build  -t poke1  -f Src/Presentation/Pokemon.API/Dockerfile ././.
```

![Screenshot](readme_files/DockerBuild.PNG)

Run the image 
```
docker run -d -p 5000:80 poke1
```

![Screenshot](readme_files/DockerRun.PNG)
  
